/*
 * Copyright (c) 2018 Felix Münchmeier
 */

package utils;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.LocalDateTime;

import static org.junit.Assert.fail;
import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOf;

public class WaitUtils extends InstanceHelper {
    private final Logger logger = Logger.getLogger(NativeDriverUtils.class);
    private final String currentUser;
    private final AppiumDriver driver;

    public WaitUtils(String user) {
        currentUser = user;
        driver = nativeDriverUtils.getNativeDriver(currentUser);
    }

    private WebDriverWait waiting() {
        return new WebDriverWait(driver, nativeDriverUtils.getTIMEOUT());
    }

    public void waitForSeconds(int Seconds) {
        LocalDateTime endTime = LocalDateTime.now().plusSeconds(Seconds);
        while (LocalDateTime.now().isBefore(endTime)) {
            driver.getOrientation();
            logger.debug("Now: " + LocalDateTime.now() + " EndTime: " + endTime);
        }
    }

    public void waitingForVisibilityOfMobileElement(MobileElement mobileElement) {
        try {
            waiting().until(visibilityOf(mobileElement));
        } catch (TimeoutException e) {
            fail(e.toString());
        }
    }

    public void waitWhileElementNotPresent(MobileElement element) {
        waitWhileElementNotPresent(element, null);
    }

    private void waitWhileElementNotPresent(MobileElement mobileElement, By byElement) {
        LocalDateTime endTime = LocalDateTime.now().plusSeconds(nativeDriverUtils.getTIMEOUT());

        if (mobileElement != null) {
            while (!getInstance(currentUser, AbstractUtils.class).isElementPresent(mobileElement) && LocalDateTime.now().isBefore(endTime)) {
                logger.debug("Now: " + LocalDateTime.now() + " EndTime: " + endTime);
            }
        }

        if (byElement != null) {
            while (!getInstance(currentUser, AbstractUtils.class).isElementPresent(byElement) && LocalDateTime.now().isBefore(endTime)) {
                logger.debug("Now: " + LocalDateTime.now() + " EndTime: " + endTime);
            }
        }
    }
}
