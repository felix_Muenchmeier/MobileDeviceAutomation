/*
 * Copyright (c) 2018 Felix Münchmeier
 */

package utils;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
import io.appium.java_client.ios.IOSDriver;
import io.appium.java_client.ios.IOSElement;
import io.appium.java_client.remote.MobileCapabilityType;
import org.apache.log4j.Logger;
import org.openqa.selenium.NoSuchSessionException;
import org.openqa.selenium.remote.DesiredCapabilities;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

public class NativeDriverUtils {
    public static final String ANDROID_OS = "Android";
    public static final String IOS_OS = "iOS";
    private static NativeDriverUtils instance = null;
    protected final String ANDROID_ACTIVITY = ".LoginActivity";
    protected final String IOS_BUNDLE_ID = "com.experitest.ExperiBank";
    protected final String ANDROID_BUNDLE_ID = "com.experitest.ExperiBank";
    private final Map<String, AppiumDriver> userDriverMap = new HashMap<>();
    private final Logger logger = Logger.getLogger(NativeDriverUtils.class);
    private final int TIMEOUT = 60;
    private final int NEW_COMMAND_TIMEOUT = TIMEOUT + 60;
    private final DesiredCapabilities dc = new DesiredCapabilities();
    private final Map<String, String> variablesFromTestMap = new HashMap<>();
    protected String scenarioName = null;

    public static NativeDriverUtils getInstanceNativeDriverUtils() {
        if (instance == null) {
            instance = new NativeDriverUtils();
        }
        return instance;
    }

    public int getTIMEOUT() {
        return TIMEOUT;
    }

    public String getAndroidActivity() {
        return ANDROID_ACTIVITY;
    }

    public void createSession(String user, String device) {
        userDriverMap.put(user, configAppiumStudioServer(user, device));
    }

    private AppiumDriver configAppiumStudioServer(String user, String device) {

        logger.info("Configuration of AppiumStudio Server User: " + user);
        AppiumDriver driver = null;

        dc.setCapability("report.disable", true);
        dc.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT, NEW_COMMAND_TIMEOUT);
        try {
            switch (device) {
                case IOS_OS:
                    driver = new IOSDriver<IOSElement>(new URL("http://localhost:4723/wd/hub"), dc);
                    break;
                case ANDROID_OS:
                    driver = new AndroidDriver<AndroidElement>(new URL("http://localhost:4723/wd/hub"), dc);
                    break;
                default:
                    logger.error("No Supported Device OS: " + device + " User: " + user);
            }
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }

        return driver;
    }

    public void stopAppiumServer() {
        for (Object userDriverEntry : userDriverMap.entrySet()) {
            try {
                Map.Entry pair = (Map.Entry) userDriverEntry;
                logger.info("Stop of AppiumStudio Server User: " + pair.getKey());
                new AppManagementHelper(pair.getKey().toString()).removeInstalledApp();
                userDriverMap.get(pair.getKey()).quit();
            } catch (NoSuchSessionException e) {
                e.printStackTrace();
            }
        }

        userDriverMap.clear();
    }

    public void setScenarioName(String Scenario) {
        scenarioName = Scenario;
    }

    String getBundleId(AppiumDriver driver) {
        String returnStr = null;

        switch (driver.getPlatformName()) {
            case IOS_OS:
                returnStr = IOS_BUNDLE_ID;
                break;
            case ANDROID_OS:
                returnStr = ANDROID_BUNDLE_ID;
                break;
        }

        return returnStr;
    }

    public AppiumDriver getNativeDriver(String user) {
        return userDriverMap.get(user);
    }

    public void setVariablesFromTestMap(String key, String value) {
        variablesFromTestMap.put(key, value);
    }

    public String getVariablesFromTestMap(String key) {
        return variablesFromTestMap.get(key);
    }

    public void printContentOfVariablesFromTestMap() {
        logger.info("Content of Map variablesFromTestMap:");

        for (Map.Entry<String, String> entry : variablesFromTestMap.entrySet()) {
            logger.info("key: " + entry.getKey() + " value: " + entry.getValue());
        }
    }
}
