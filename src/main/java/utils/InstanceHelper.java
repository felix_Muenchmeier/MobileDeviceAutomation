/*
 * Copyright (c) 2018 Felix Münchmeier
 */

package utils;

import java.lang.reflect.InvocationTargetException;
import java.util.HashMap;
import java.util.Map;

import static utils.NativeDriverUtils.getInstanceNativeDriverUtils;

public class InstanceHelper {
    private static final Map<String, Object> instanceMap = new HashMap<>();

    protected final NativeDriverUtils nativeDriverUtils = getInstanceNativeDriverUtils();

    protected static void clearInstanceMap() {
        instanceMap.clear();
    }

    protected <T extends InstanceHelper> T getInstance(String currentUser, Class<T> type) {
        String classNameIdentifier = type.getTypeName() + currentUser;
        instanceMap.putIfAbsent(classNameIdentifier, createInstance(currentUser, type));
        return (T) instanceMap.get(classNameIdentifier);
    }

    private <T extends InstanceHelper> T createInstance(String currentUser, Class<T> type) {
        try {
            return type.getDeclaredConstructor(String.class).newInstance(currentUser);
        } catch (InstantiationException | IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
            throw new IllegalArgumentException("Unable to create an instance of class " + type.getSimpleName(),
                    e.getCause());
        }
    }
}
