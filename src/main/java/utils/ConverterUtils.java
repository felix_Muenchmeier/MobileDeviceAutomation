/*
 * Copyright (c) 2018 Felix Münchmeier
 */

package utils;

import io.appium.java_client.MobileBy;
import io.appium.java_client.MobileElement;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;

public class ConverterUtils extends InstanceHelper {
    private final Logger logger = Logger.getLogger(NativeDriverUtils.class);
    private final String currentUser;

    public ConverterUtils(String user) {
        currentUser = user;
    }

    public String convertElementToXpath(MobileElement element) {
        return convertElementToXpath(element, null);
    }

    private String convertElementToXpath(MobileElement mobileElement, By byElement) {
        String returnString = null;
        By element = byElement;

        if (mobileElement != null) {
            element = convertMobileElementToBy(mobileElement);
        }


        if (element.toString().contains("xpath")) {
            returnString = element.toString().replace("By.xpath: ", "");
        }

        if (element.toString().contains("AccessibilityId")) {
            returnString = "//*[@accessibilityLabel='" + element.toString().replace("By.AccessibilityId: ", "") + "']";
        }

        if (element.toString().contains("id")) {
            returnString = "//*[@id='" + element.toString().replace("By.id: ", "") + "']";
        }


        if (mobileElement != null) {
            logger.info(currentUser + " convert MobileElement to Xpath: " + mobileElement.toString() + " To: " + returnString);
        } else {
            logger.info(currentUser + " convert By to Xpath: " + byElement.toString() + " To: " + returnString);
        }

        return returnString;
    }

    public By convertMobileElementToBy(MobileElement element) {
        By returnBy = null;

        if (element.toString().contains("xpath")) {
            returnBy = By.xpath(element.toString().replace("Located by By.chained({By.xpath: ", "").replace("})", ""));
        }

        if (element.toString().contains("AccessibilityId")) {
            returnBy = MobileBy.AccessibilityId(element.toString().replace("Located by By.chained({By.AccessibilityId: ", "").replace("})", ""));
        }

        if (element.toString().contains("id")) {
            returnBy = By.id(element.toString().replace("Located by By.chained({By.id: ", "").replace("})", ""));
        }

        logger.info(currentUser + " convert MobileElement to By: " + element.toString() + " To: " + returnBy);

        return returnBy;
    }
}
