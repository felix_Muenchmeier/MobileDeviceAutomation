/*
 * Copyright (c) 2018 Felix Münchmeier
 */

package utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class SelectUserHelper extends InstanceHelper {
    private final String currentUser;

    public SelectUserHelper(String user) {
        currentUser = user;
    }

    private String selectUser(String user) {
        if (user == null) {
            throw new IllegalArgumentException("*********** No User Specified User: " + currentUser + " ************");
        }

        String userStr = getPropertiesByName(user);

        if (userStr == null) {
            throw new IllegalArgumentException("*********** No Supported User Specified User: " + currentUser + " ************");
        }

        return userStr;
    }

    public String getUserName() {
        return selectUser(currentUser + "Name");
    }

    public String getUserPassword() {
        return selectUser(currentUser + "Password");
    }

    private String getPropertiesByName(String propName) {
        Properties prop = new Properties();
        InputStream input;

        try {
            input = getClass().getClassLoader().getResourceAsStream("users/users.properties");
            prop.load(input);
            input.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return prop.getProperty(propName);
    }
}
