/*
 * Copyright (c) 2018 Felix Münchmeier
 */

package utils;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.HowToUseLocators;
import io.appium.java_client.pagefactory.iOSFindBy;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.support.PageFactory;

import java.time.LocalDateTime;

import static io.appium.java_client.pagefactory.LocatorGroupStrategy.ALL_POSSIBLE;

public class AbstractUtils extends InstanceHelper {
    private final String currentUser;
    private final AppiumDriver driver;
    public int screenWidth;
    public int screenHeight;
    @HowToUseLocators(iOSAutomation = ALL_POSSIBLE)
    @iOSFindBy(id = "Back")
    @iOSFindBy(id = "Profiles & Device Management")
    protected MobileElement backButton;
    @iOSFindBy(id = "Yes")
    @AndroidFindBy(id = "button1")
    protected MobileElement yesButton;

    public AbstractUtils(String user) {
        currentUser = user;
        driver = nativeDriverUtils.getNativeDriver(currentUser);
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
        getScreenSize();
    }

    public boolean isElementPresent(MobileElement element) {
        return isElementPresent(element, null);
    }

    public boolean isElementPresent(By element) {
        return isElementPresent(null, element);
    }

    private boolean isElementPresent(MobileElement mobileElement, By byElement) {
        try {
            if (mobileElement != null) {
                return mobileElement.isDisplayed();
            }

            if (byElement != null) {
                return driver.findElement(byElement).isDisplayed();
            }
        } catch (NoSuchElementException e) {
            return false;
        } catch (WebDriverException e) {
            return false;
        }
        return false;
    }

    public void clickOnBackButton() {
        backButton.click();
    }

    public void clickOnYesButton() {
        getInstance(currentUser, WaitUtils.class).waitWhileElementNotPresent(yesButton);
        yesButton.click();
    }

    public void clickElementATillElementBIsPresent(MobileElement elementToClick, MobileElement elementToWaitFor) {
        LocalDateTime endTime = LocalDateTime.now().plusSeconds(nativeDriverUtils.getTIMEOUT());

        while (!isElementPresent(elementToWaitFor) && LocalDateTime.now().isBefore(endTime)) {
            getInstance(currentUser, WaitUtils.class).waitForSeconds(1);

            if (isElementPresent(elementToClick)) {
                elementToClick.click();
            }
        }
    }

    private void getScreenSize() {
        screenWidth = driver.manage().window().getSize().width;
        screenHeight = driver.manage().window().getSize().height;
    }

    public void hideKeyboard() {
        if (driver.getPlatformName().equals(NativeDriverUtils.ANDROID_OS)) {
            driver.hideKeyboard();
        }
    }
}
