/*
 * Copyright (c) 2018 Felix Münchmeier
 */

package utils;

import com.experitest.appium.SeeTestClient;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.android.Activity;
import io.appium.java_client.android.AndroidDriver;

public class AppiumStudioHelper extends InstanceHelper {
    private static final int DELAY_BETWEEN_SCROLLS = 1000;
    private static final int ROUNDS_FOR_SCROLLS = 10;
    public static int offSetForDisplay;
    private static int swipeDuration;
    private final AppiumDriver driver;
    private final String currentUser;

    public AppiumStudioHelper(String user) {
        currentUser = user;
        driver = nativeDriverUtils.getNativeDriver(currentUser);
        setOffSetForDisplayAndSwipeDuration();
    }

    private SeeTestClient setSeeTestClient() {
        return new SeeTestClient(driver);
    }

    public void launchAppIos(String bundle_id) {
        setSeeTestClient().launch(bundle_id, false, true);
    }

    public void launchAppAndroid(String bundle_id, String android_activity) {
        Activity activity = new Activity(bundle_id, android_activity);
        ((AndroidDriver) driver).startActivity(activity);
    }

    public void swipeToElement(MobileElement element, swipeDirection direction, int elementToFindIndex, boolean click) {
        // https://docs.experitest.com/display/public/TD/SeeTestAutomation-+SetProperty
        setSeeTestClient().setProperty("element.visibility.level", "full");

        setSeeTestClient().swipeWhileNotFound(direction.toString(),
                offSetForDisplay,
                swipeDuration,
                "NATIVE",
                "xpath=" + getInstance(currentUser, ConverterUtils.class).convertElementToXpath(element),
                elementToFindIndex,
                DELAY_BETWEEN_SCROLLS,
                ROUNDS_FOR_SCROLLS,
                click);
    }

    private void deviceAction(deviceActions action) {
        setSeeTestClient().deviceAction(action.toString());
    }

    public void pressHomeButton() {
        deviceAction(deviceActions.Home);
    }

    public void unInstallApp() {
        setSeeTestClient().uninstall(nativeDriverUtils.getBundleId(driver));
    }

    private void setOffSetForDisplayAndSwipeDuration() {
        // http://appium.experitest.com/t/swipe-question-s/786/6
        offSetForDisplay = driver.manage().window().getSize().getHeight() / 2; // start from mid screen
        swipeDuration = new Double(driver.manage().window().getSize().getHeight() * 0.3).intValue(); // just an example
    }

    public enum swipeDirection {
        Up, Down, Left, Right
    }

    public enum deviceActions {
        Home, Back, Power, Wake, Landscape, Portrait, Change_Orientation, Menu, Unlock, Paste, Volume_Up, Volume_Down, Recent_Apps
    }
}
