/*
 * Copyright (c) 2018 Felix Münchmeier
 */

package utils;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;
import org.apache.log4j.Logger;
import org.openqa.selenium.support.PageFactory;

import static org.junit.Assert.assertTrue;
import static utils.NativeDriverUtils.ANDROID_OS;
import static utils.NativeDriverUtils.IOS_OS;

public class AppManagementHelper extends InstanceHelper {
    private final Logger logger = Logger.getLogger(AppManagementHelper.class);
    private final String currentUser;
    private final AppiumDriver driver;
    @iOSFindBy(id = "Settings")
    protected MobileElement iOSSettings;
    @iOSFindBy(id = "General")
    protected MobileElement iOSGeneral;
    @iOSFindBy(id = "Profiles & Device Management")
    protected MobileElement profilesAndDeviceManagement;
    @iOSFindBy(id = "Experitest Ltd")
    protected MobileElement developerExperitestLtd;
    @iOSFindBy(id = "Trust “Experitest Ltd”")
    protected MobileElement trustExperitestButton;
    @iOSFindBy(id = "Trust")
    protected MobileElement trustButton;
    @iOSFindBy(id = "Delete App")
    protected MobileElement deleteAppButton;
    @iOSFindBy(id = "About")
    protected MobileElement aboutButton;
    @iOSFindBy(xpath = "//*[@text='Settings' and @class='UIAButton']")
    protected MobileElement closeIOSGeneralButton;
    @iOSFindBy(id = "Verified")
    protected MobileElement verified;

    public AppManagementHelper(String user) {
        currentUser = user;
        driver = nativeDriverUtils.getNativeDriver(currentUser);
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

    public void removeInstalledApp() {
        if (driver.isAppInstalled(nativeDriverUtils.getBundleId(driver))) {
            logger.info("Uninstall App for User: " + currentUser);
            getInstance(currentUser, AppiumStudioHelper.class).unInstallApp();
            logger.info("App Uninstalled for User: " + currentUser);
        }
    }

    public void installApp() {
        logger.info("Install App for User: " + currentUser);

        switch (driver.getPlatformName()) {
            case IOS_OS:
                driver.installApp(nativeDriverUtils.IOS_BUNDLE_ID);
                break;
            case ANDROID_OS:
                driver.installApp(nativeDriverUtils.ANDROID_BUNDLE_ID + "/" + nativeDriverUtils.ANDROID_ACTIVITY);
                break;
        }

        logger.info("App Installed for User: " + currentUser);
    }

    public void launchApp() {
        switch (driver.getPlatformName()) {
            case IOS_OS:
                trustIOSDeveloper();
                getInstance(currentUser, AppiumStudioHelper.class).launchAppIos(nativeDriverUtils.getBundleId(driver));
                break;
            case ANDROID_OS:
                getInstance(currentUser, AppiumStudioHelper.class).launchAppAndroid(nativeDriverUtils.getBundleId(driver), nativeDriverUtils.getAndroidActivity());
                break;
        }
    }

    public void trustIOSDeveloper() {
        openiOSSettings();
        openiOSGeneral();
        openIOSProfilesAndDeviceManagement();
        clickOpenDeveloperExperitestLtd();
        clickOnTrustExperitestButton();
        closeDeveloperExperitestLtd();
        closeIOSProfilesAndDeviceManagement();
        closeIOSGeneral();
        closeIOSSettings();
    }

    public void openiOSSettings() {
        getInstance(currentUser, AppiumStudioHelper.class).pressHomeButton();
        iOSSettings.click();
        assertTrue(getInstance(currentUser, AbstractUtils.class).isElementPresent(iOSSettings));
    }

    public void closeIOSSettings() {
        getInstance(currentUser, AppiumStudioHelper.class).pressHomeButton();
    }

    public void openiOSGeneral() {
        getInstance(currentUser, AppiumStudioHelper.class).swipeToElement(iOSGeneral, AppiumStudioHelper.swipeDirection.Down, 0, false);
        getInstance(currentUser, AbstractUtils.class).clickElementATillElementBIsPresent(iOSGeneral, aboutButton);
        assertTrue(getInstance(currentUser, AbstractUtils.class).isElementPresent(aboutButton));
    }

    public void closeIOSGeneral() {
        getInstance(currentUser, WaitUtils.class).waitWhileElementNotPresent(closeIOSGeneralButton);
        closeIOSGeneralButton.click();
        assertTrue(getInstance(currentUser, AbstractUtils.class).isElementPresent(iOSSettings));
    }

    public void openIOSProfilesAndDeviceManagement() {
        getInstance(currentUser, AppiumStudioHelper.class).swipeToElement(profilesAndDeviceManagement, AppiumStudioHelper.swipeDirection.Down, 0, false);
        getInstance(currentUser, AbstractUtils.class).clickElementATillElementBIsPresent(profilesAndDeviceManagement, developerExperitestLtd);
        assertTrue(getInstance(currentUser, AbstractUtils.class).isElementPresent(profilesAndDeviceManagement));
    }

    public void closeIOSProfilesAndDeviceManagement() {
        getInstance(currentUser, WaitUtils.class).waitWhileElementNotPresent(iOSGeneral);
        iOSGeneral.click();
        assertTrue(getInstance(currentUser, AbstractUtils.class).isElementPresent(iOSGeneral));
    }

    public void clickOpenDeveloperExperitestLtd() {
        getInstance(currentUser, AppiumStudioHelper.class).swipeToElement(developerExperitestLtd, AppiumStudioHelper.swipeDirection.Down, 0, false);
        getInstance(currentUser, AbstractUtils.class).clickElementATillElementBIsPresent(developerExperitestLtd, verified);
        assertTrue(getInstance(currentUser, AbstractUtils.class).isElementPresent(developerExperitestLtd));
    }

    public void closeDeveloperExperitestLtd() {
        getInstance(currentUser, AbstractUtils.class).clickOnBackButton();
        assertTrue(getInstance(currentUser, AbstractUtils.class).isElementPresent(developerExperitestLtd));
    }

    public void clickOnTrustExperitestButton() {
        getInstance(currentUser, WaitUtils.class).waitWhileElementNotPresent(verified);
        if (getInstance(currentUser, AbstractUtils.class).isElementPresent(trustExperitestButton)) {
            trustExperitestButton.click();
            getInstance(currentUser, WaitUtils.class).waitWhileElementNotPresent(trustButton);
            trustButton.click();
            getInstance(currentUser, WaitUtils.class).waitWhileElementNotPresent(deleteAppButton);
        }
    }
}
