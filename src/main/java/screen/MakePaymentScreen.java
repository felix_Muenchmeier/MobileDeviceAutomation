/*
 * Copyright (c) 2018 Felix Münchmeier
 */

package screen;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;
import org.openqa.selenium.support.PageFactory;
import utils.InstanceHelper;
import utils.WaitUtils;

public class MakePaymentScreen extends InstanceHelper {
    private final String currentUser;
    private final AppiumDriver driver;
    @iOSFindBy(id = "makePaymentView")
    @AndroidFindBy(id = "makePaymentView")
    protected MobileElement makePaymentView;
    @iOSFindBy(id = "phoneTextField")
    @AndroidFindBy(id = "phoneTextField")
    protected MobileElement phoneTextField;
    @iOSFindBy(id = "nameTextField")
    @AndroidFindBy(id = "nameTextField")
    protected MobileElement nameTextField;
    @iOSFindBy(id = "amountTextField")
    @AndroidFindBy(id = "amountTextField")
    protected MobileElement amountTextField;
    @iOSFindBy(id = "countryTextField")
    @AndroidFindBy(id = "countryTextField")
    protected MobileElement countryTextField;
    @iOSFindBy(id = "sendPaymentButton")
    @AndroidFindBy(id = "sendPaymentButton")
    protected MobileElement sendPaymentButton;
    @iOSFindBy(id = "cancelButton")
    @AndroidFindBy(id = "cancelButton")
    protected MobileElement cancelButton;

    public MakePaymentScreen(String user) {
        currentUser = user;
        driver = nativeDriverUtils.getNativeDriver(currentUser);
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

    public boolean isOnMakePaymentScreen() {
        getInstance(currentUser, WaitUtils.class).waitingForVisibilityOfMobileElement(makePaymentView);
        return makePaymentView.isDisplayed();
    }

    public void enterReceiverPhoneNumber(String phoneNumber) {
        phoneTextField.click();
        phoneTextField.sendKeys(phoneNumber);
    }

    public void enterReceiverName(String name) {
        nameTextField.click();
        nameTextField.sendKeys(name);
    }

    public void enterAmountToSend(String amount) {
        amountTextField.click();
        amountTextField.sendKeys(amount);
    }

    public void enterCountry(String country) {
        countryTextField.click();
        countryTextField.sendKeys(country);
    }

    public void clickOnSendPaymentButton() {
        sendPaymentButton.click();
    }

    public void clickOnCancelButton() {
        cancelButton.click();
    }
}
