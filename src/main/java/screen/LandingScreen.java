/*
 * Copyright (c) 2018 Felix Münchmeier
 */

package screen;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;
import org.openqa.selenium.support.PageFactory;
import utils.AbstractUtils;
import utils.InstanceHelper;
import utils.WaitUtils;

public class LandingScreen extends InstanceHelper {
    private final String currentUser;
    private final AppiumDriver driver;
    @iOSFindBy(accessibility = "logo.png")
    @AndroidFindBy(xpath = "//*[@class='android.widget.ImageView' and @width>0 and ./parent::*[@id='makePaymentView']]")
    protected MobileElement logo;
    @iOSFindBy(id = "loginButton")
    @AndroidFindBy(id = "loginButton")
    protected MobileElement loginButton;
    @iOSFindBy(id = "usernameTextField")
    @AndroidFindBy(id = "usernameTextField")
    protected MobileElement usernameTextField;
    @iOSFindBy(id = "passwordTextField")
    @AndroidFindBy(id = "passwordTextField")
    protected MobileElement passwordTextField;

    public LandingScreen(String user) {
        currentUser = user;
        driver = nativeDriverUtils.getNativeDriver(currentUser);
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

    public void clickOnLoginButton() {
        loginButton.click();
    }

    public boolean isOnLandingScreen() {
        getInstance(currentUser, WaitUtils.class).waitingForVisibilityOfMobileElement(logo);
        return logo.isDisplayed();
    }

    public void enterUsername(String userName) {
        usernameTextField.click();
        usernameTextField.click();
        usernameTextField.sendKeys(userName);
        getInstance(currentUser, AbstractUtils.class).hideKeyboard();
    }

    public void enterPassword(String password) {
        passwordTextField.click();
        passwordTextField.clear();
        passwordTextField.sendKeys(password);
        getInstance(currentUser, AbstractUtils.class).hideKeyboard();
    }
}
