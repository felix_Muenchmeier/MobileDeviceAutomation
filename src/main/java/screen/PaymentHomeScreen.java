/*
 * Copyright (c) 2018 Felix Münchmeier
 */

package screen;

import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.pagefactory.AndroidFindBy;
import io.appium.java_client.pagefactory.AppiumFieldDecorator;
import io.appium.java_client.pagefactory.iOSFindBy;
import org.openqa.selenium.support.PageFactory;
import utils.InstanceHelper;
import utils.WaitUtils;

public class PaymentHomeScreen extends InstanceHelper {
    private final String currentUser;
    private final AppiumDriver driver;
    @iOSFindBy(id = "paymentHomeView")
    @AndroidFindBy(id = "paymentHomeView")
    protected MobileElement paymentHomeView;
    @iOSFindBy(id = "logoutButton")
    @AndroidFindBy(id = "logoutButton")
    protected MobileElement logoutButton;
    @iOSFindBy(id = "makePaymentButton")
    @AndroidFindBy(id = "makePaymentButton")
    protected MobileElement makePaymentButton;
    @iOSFindBy(xpath = "(//*/*[@class='UIAStaticText'])[2]")
    @AndroidFindBy(xpath = "//*[@text and @class='android.view.View']")
    protected MobileElement currentBalance;

    public PaymentHomeScreen(String user) {
        currentUser = user;
        driver = nativeDriverUtils.getNativeDriver(currentUser);
        PageFactory.initElements(new AppiumFieldDecorator(driver), this);
    }

    public boolean isLoggedin() {
        getInstance(currentUser, WaitUtils.class).waitWhileElementNotPresent(paymentHomeView);
        return paymentHomeView.isDisplayed();
    }

    public void clickOnLogoutButton() {
        logoutButton.click();
    }

    public void clickOnMakePaymentButton() {
        makePaymentButton.click();
    }

    public String getCurrentBalance() {
        getInstance(currentUser, WaitUtils.class).waitWhileElementNotPresent(currentBalance);
        return currentBalance.getText().replaceAll("[^\\d+]", "");
    }
}