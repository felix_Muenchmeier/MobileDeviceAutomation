/*
 * Copyright (c) 2018 Felix Münchmeier
 */

package stepdefsUi.hooks;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import org.apache.log4j.Logger;
import utils.InstanceHelper;

public class ScenarioHooks extends InstanceHelper {
    private final Logger logger = Logger.getLogger(ScenarioHooks.class);

    @Before
    public void setup(Scenario scenario) {
        nativeDriverUtils.setScenarioName(scenario.getName());
        logger.info("Start of Scenario: " + scenario.getName());
    }

    @After
    public void teardown(Scenario scenario) {
        nativeDriverUtils.stopAppiumServer();
        clearInstanceMap();

        logger.info("End of Scenario: " + scenario.getName());
    }
}
