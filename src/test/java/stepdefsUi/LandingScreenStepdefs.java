/*
 * Copyright (c) 2018 Felix Münchmeier
 */

package stepdefsUi;

import cucumber.api.java.en.Then;
import screen.LandingScreen;
import utils.InstanceHelper;

import static org.junit.Assert.assertTrue;

public class LandingScreenStepdefs extends InstanceHelper {

    @Then("^\"([^\"]*)\" verifies that the App is open and he is at the Landing screen$")
    public void verifiesThatTheAppIsOpenAndHeIsAtTheLandingScreen(String user) {
        verifiesThatHeIsAtTheLandingScreen(user);
    }

    @Then("^\"([^\"]*)\" clicks on the Login button at the Landing screen$")
    public void clicksOnTheLoginButtonAtTheLandingScreen(String user) {
        getInstance(user, LandingScreen.class).clickOnLoginButton();
    }

    @Then("^\"([^\"]*)\" verifies that he is at the Landing Screen$")
    public void verifiesThatHeIsAtTheLandingScreen(String user) {
        assertTrue(getInstance(user, LandingScreen.class).isOnLandingScreen());
    }
}
