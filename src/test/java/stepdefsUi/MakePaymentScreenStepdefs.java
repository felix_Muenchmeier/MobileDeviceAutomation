/*
 * Copyright (c) 2018 Felix Münchmeier
 */

package stepdefsUi;

import cucumber.api.java.en.Then;
import screen.MakePaymentScreen;
import utils.AbstractUtils;
import utils.InstanceHelper;

public class MakePaymentScreenStepdefs extends InstanceHelper {

    @Then("^\"([^\"]*)\" enters the receiver Phone Number \"([^\"]*)\"$")
    public void entersTheReceiverPhoneNumber(String user, String receiverPhoneNumber) {
        getInstance(user, MakePaymentScreen.class).enterReceiverPhoneNumber(receiverPhoneNumber);
        getInstance(user, AbstractUtils.class).hideKeyboard();
    }

    @Then("^\"([^\"]*)\" enters the receiver Name \"([^\"]*)\"$")
    public void entersTheReceiverName(String user, String receiverName) {
        getInstance(user, MakePaymentScreen.class).enterReceiverName(receiverName);
        getInstance(user, AbstractUtils.class).hideKeyboard();
    }

    @Then("^\"([^\"]*)\" enters \"([^\"]*)\" as the Amount to send$")
    public void entersAsTheAmountToSend(String user, String amount) {
        getInstance(user, MakePaymentScreen.class).enterAmountToSend(amount);
        getInstance(user, AbstractUtils.class).hideKeyboard();
    }

    @Then("^\"([^\"]*)\" enters \"([^\"]*)\" as Country$")
    public void selectsAsCountry(String user, String country) {
        getInstance(user, MakePaymentScreen.class).enterCountry(country);
        getInstance(user, AbstractUtils.class).hideKeyboard();
    }

    @Then("^\"([^\"]*)\" clicks on Send Payment Button$")
    public void clicksOnSendPaymentButton(String user) {
        getInstance(user, MakePaymentScreen.class).clickOnSendPaymentButton();
    }

    @Then("^\"([^\"]*)\" confirms Payment$")
    public void confirmsPayment(String user) {
        getInstance(user, AbstractUtils.class).clickOnYesButton();
    }

    @Then("^\"([^\"]*)\" clicks on Cancel Button$")
    public void clicksOnCancelButton(String user) {
        getInstance(user, MakePaymentScreen.class).clickOnCancelButton();
    }
}
