/*
 * Copyright (c) 2018 Felix Münchmeier
 */

package stepdefsUi;

import cucumber.api.java.en.Then;
import screen.LandingScreen;
import screen.PaymentHomeScreen;
import utils.InstanceHelper;
import utils.SelectUserHelper;

import static org.junit.Assert.assertTrue;

public class LoginStepdefs extends InstanceHelper {

    @Then("^\"([^\"]*)\" enters his Username$")
    public void entersHisUserName(String user) {
        String userName = getInstance(user, SelectUserHelper.class).getUserName();

        nativeDriverUtils.setVariablesFromTestMap("UserName" + user, userName);
        entersTheUserName(user, userName);
    }

    @Then("^\"([^\"]*)\" enters his Password$")
    public void entersHisPassword(String user) {
        String password = getInstance(user, SelectUserHelper.class).getUserPassword();

        nativeDriverUtils.setVariablesFromTestMap("Password" + user, password);
        entersThePassword(user, password);
    }

    @Then("^\"([^\"]*)\" verifies that he is logged in$")
    public void verifiesThatHeIsLoggedIn(String user) {
        assertTrue(getInstance(user, PaymentHomeScreen.class).isLoggedin());
    }

    @Then("^\"([^\"]*)\" does a Login with UserName: \"([^\"]*)\" and Password: \"([^\"]*)\"$")
    public void doesALoginWithUserNameAndPassword(String user, String userName, String userPassword) {
        LandingScreenStepdefs landingScreenStepdefs = new LandingScreenStepdefs();

        entersTheUserName(user, userName);
        entersThePassword(user, userPassword);
        landingScreenStepdefs.clicksOnTheLoginButtonAtTheLandingScreen(user);
        verifiesThatHeIsLoggedIn(user);
    }

    @Then("^\"([^\"]*)\" does a Login$")
    public void doesALogin(String user) {
        doesALoginWithUserNameAndPassword(user, getInstance(user, SelectUserHelper.class).getUserName(), getInstance(user, SelectUserHelper.class).getUserPassword());
    }

    @Then("^\"([^\"]*)\" enters the Username: \"([^\"]*)\"$")
    public void entersTheUserName(String user, String userName) {
        getInstance(user, LandingScreen.class).enterUsername(userName);
    }

    @Then("^\"([^\"]*)\" enters the Password: \"([^\"]*)\"$")
    public void entersThePassword(String user, String password) {
        getInstance(user, LandingScreen.class).enterPassword(password);
    }
}
