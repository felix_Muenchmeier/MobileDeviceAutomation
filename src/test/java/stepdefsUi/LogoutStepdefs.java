/*
 * Copyright (c) 2018 Felix Münchmeier
 */

package stepdefsUi;

import cucumber.api.java.en.Then;
import screen.LandingScreen;
import utils.InstanceHelper;

import static org.junit.Assert.assertTrue;

public class LogoutStepdefs extends InstanceHelper {

    @Then("^\"([^\"]*)\" verifies that he is logged out from the App$")
    public void verifiesThatHeIsLoggedOutFromTheApp(String user) {
        assertTrue(getInstance(user, LandingScreen.class).isOnLandingScreen());
    }

    @Then("^\"([^\"]*)\" does a Logout$")
    public void doesALogOut(String user) {
        PaymentHomeScreenStepdefs paymentHomeScreenStepdefs = new PaymentHomeScreenStepdefs();

        paymentHomeScreenStepdefs.clicksOnTheLogoutButton(user);
        verifiesThatHeIsLoggedOutFromTheApp(user);
    }
}
