/*
 * Copyright (c) 2018 Felix Münchmeier
 */

package stepdefsUi;

import cucumber.api.java.en.Then;
import screen.MakePaymentScreen;
import screen.PaymentHomeScreen;
import utils.InstanceHelper;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class PaymentHomeScreenStepdefs extends InstanceHelper {

    @Then("^\"([^\"]*)\" clicks on the Logout button$")
    public void clicksOnTheLogoutButton(String user) {
        getInstance(user, PaymentHomeScreen.class).clickOnLogoutButton();
    }

    @Then("^\"([^\"]*)\" clicks on Make Payment Button$")
    public void clicksOnMakePaymentButton(String user) {
        getInstance(user, PaymentHomeScreen.class).clickOnMakePaymentButton();
        assertTrue(getInstance(user, MakePaymentScreen.class).isOnMakePaymentScreen());
    }

    @Then("^\"([^\"]*)\" gets the Balance before a Payment$")
    public void checksTheBalanceBeforeAPayment(String user) {
        nativeDriverUtils.setVariablesFromTestMap("BalanceBeforeAPayment" + user, getInstance(user, PaymentHomeScreen.class).getCurrentBalance());
    }

    @Then("^\"([^\"]*)\" checks that the Balance is lower after a Payment$")
    public void checksThatTheBalanceIsLowerAfterAPayment(String user) {
        String balanceBefore = nativeDriverUtils.getVariablesFromTestMap("BalanceBeforeAPayment" + user);
        String balanceAfter = getInstance(user, PaymentHomeScreen.class).getCurrentBalance();

        assertTrue(
                Integer.parseInt(balanceAfter)
                        <
                        Integer.parseInt(balanceBefore));
    }

    @Then("^\"([^\"]*)\" checks that the Balance is equal to the Balance before$")
    public void checksThatTheBalanceIsEqualToTheBalanceBefore(String user) {
        String balanceBefore = nativeDriverUtils.getVariablesFromTestMap("BalanceBeforeAPayment" + user);
        String balanceAfter = getInstance(user, PaymentHomeScreen.class).getCurrentBalance();

        assertEquals(balanceBefore, balanceAfter);
    }
}
