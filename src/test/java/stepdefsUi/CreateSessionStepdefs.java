/*
 * Copyright (c) 2018 Felix Münchmeier
 */

package stepdefsUi;

import cucumber.api.java.en.Then;
import utils.AppManagementHelper;
import utils.InstanceHelper;

public class CreateSessionStepdefs extends InstanceHelper {

    @Then("^Create new Session for \"([^\"]*)\" Device: \"([^\"]*)\"$")
    public void createNewSession(String user, String device) {
        nativeDriverUtils.createSession(user, device);

        getInstance(user, AppManagementHelper.class).installApp();
        getInstance(user, AppManagementHelper.class).launchApp();
    }
}
