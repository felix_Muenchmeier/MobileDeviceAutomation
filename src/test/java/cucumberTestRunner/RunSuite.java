/*
 * Copyright (c) 2018 Felix Münchmeier
 */

package cucumberTestRunner;

import org.junit.AfterClass;
import org.junit.Test;
import org.junit.runner.JUnitCore;
import utils.NativeDriverUtils;

import static utils.NativeDriverUtils.getInstanceNativeDriverUtils;

public class RunSuite {
    private static final NativeDriverUtils nativeDriverUtils = getInstanceNativeDriverUtils();

    @AfterClass
    public static void updateTestExecutionSummary() {
        nativeDriverUtils.printContentOfVariablesFromTestMap();
    }

    @Test
    public void testAllCucumberTests() {
        JUnitCore.runClasses(RunCucumberTests.class);
    }
}
