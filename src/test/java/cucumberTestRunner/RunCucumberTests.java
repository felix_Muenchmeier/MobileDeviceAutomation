/*
 * Copyright (c) 2018 Felix Münchmeier
 */

package cucumberTestRunner;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.AfterClass;
import org.junit.runner.RunWith;
import utils.InstanceHelper;
import utils.NativeDriverUtils;

import static utils.NativeDriverUtils.getInstanceNativeDriverUtils;

@RunWith(Cucumber.class)
@CucumberOptions(
        strict = true,
        plugin = {"pretty", "html:target/cucumber", "json:target/cucumber/report.json"},
        tags = {"~@Wip"},
        features = "./features",
        glue = {"stepdefsUi"}
)

public class RunCucumberTests extends InstanceHelper {
    private static final NativeDriverUtils nativeDriverUtils = getInstanceNativeDriverUtils();

    @AfterClass
    public static void stopAppium() {
        nativeDriverUtils.stopAppiumServer();
        clearInstanceMap();
    }
}
