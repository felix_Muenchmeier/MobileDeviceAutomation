# Mobile Device Automation Test Framework

This is a framework for UI test automation on native devices.

# Tools 

* [Java](https://www.java.com) - Main language to write tests
* [Maven](https://maven.apache.org) - Dependencey Managment
* [Cucumber](https://cucumber.io) - BDD framework for test automation 
* [Junit](http://junit.org) - Framework which uses annotations to identify methods that specify a test
* [Appium Studio](https://experitest.com/mobile-test-automation/appium-studio) - Appium Studio is a framework for Mobile test automation for Android and iOS

# Mobile Device tests
## How to run tests for Mobile devices

### 1. Precondition
Tool should be installed before:
- Java
- Git
- Maven
- AppiumStudio

For iOS:
- Apple Developer Account
- Device added to the Developer Account

For iOS and Android:
- The Device you want to use is Added to Appium Studio and Appium Studio is running

### 2. Clone repository 
```
git clone https://gitlab.com/felix_Muenchmeier/MobileDeviceAutomation.git
```
### 3. Open proper directory
```
cd MobileDeviceAutomation
``` 
### 4. Run tests
```
mvn clean test -P MobileDeviceTest
```
