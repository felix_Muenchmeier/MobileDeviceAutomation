#
# Copyright (c) 2018 Felix Münchmeier
#

Feature: all Payment related Tests

  Background:
    Given Create new Session for "UserA" Device: "iOS"
    Given "UserA" verifies that the App is open and he is at the Landing screen
    Given "UserA" does a Login

    #Payment Test
  @iOS
  Scenario: Payment
    When "UserA" gets the Balance before a Payment
    And "UserA" clicks on Make Payment Button
    And "UserA" enters the receiver Phone Number "9012"
    And "UserA" enters the receiver Name "Commander Susan Ivanova"
    And "UserA" enters "64" as the Amount to send
    And "UserA" enters "Russia" as Country
    And "UserA" clicks on Send Payment Button
    And "UserA" confirms Payment
    Then "UserA" checks that the Balance is lower after a Payment
    Then "UserA" does a Logout

    #Cancel Payment Test
  #@iOS
  Scenario: Cancel Payment
    When "UserA" gets the Balance before a Payment
    And "UserA" clicks on Make Payment Button
    And "UserA" enters the receiver Phone Number "3456"
    And "UserA" enters the receiver Name "Ijon Tichy"
    And "UserA" enters "92" as the Amount to send
    And "UserA" enters "Germany" as Country
    And "UserA" clicks on Cancel Button
    Then "UserA" checks that the Balance is equal to the Balance before
    Then "UserA" does a Logout