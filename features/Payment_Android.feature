#
# Copyright (c) 2018 Felix Münchmeier
#

Feature: all Payment related Tests

  Background:
    Given Create new Session for "UserA" Device: "Android"
    Given "UserA" verifies that the App is open and he is at the Landing screen
    Given "UserA" does a Login

    #Payment Test
  @Android
  Scenario: Payment
    When "UserA" gets the Balance before a Payment
    And "UserA" clicks on Make Payment Button
    And "UserA" enters the receiver Phone Number "1234"
    And "UserA" enters the receiver Name "Doctor Who"
    And "UserA" enters "5" as the Amount to send
    And "UserA" enters "United Kingdom" as Country
    And "UserA" clicks on Send Payment Button
    And "UserA" confirms Payment
    Then "UserA" checks that the Balance is lower after a Payment
    Then "UserA" does a Logout

    #Cancel Payment Test
  @Android
  Scenario: Cancel Payment
    When "UserA" gets the Balance before a Payment
    And "UserA" clicks on Make Payment Button
    And "UserA" enters the receiver Phone Number "1701"
    And "UserA" enters the receiver Name "Captain Jean-Luc Picard"
    And "UserA" enters "30" as the Amount to send
    And "UserA" enters "France" as Country
    And "UserA" clicks on Cancel Button
    Then "UserA" checks that the Balance is equal to the Balance before
    Then "UserA" does a Logout