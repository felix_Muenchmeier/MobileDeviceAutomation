#
# Copyright (c) 2018 Felix Münchmeier
#

Feature: all Login and Logout related Tests

  Background:
    Given Create new Session for "UserA" Device: "iOS"
    Given "UserA" verifies that the App is open and he is at the Landing screen

	#Login and Logout Test
  @iOS
  Scenario: Login and Logout
    When "UserA" enters his Username
    And "UserA" enters his Password
    And "UserA" clicks on the Login button at the Landing screen
    Then "UserA" verifies that he is logged in
    When "UserA" clicks on the Logout button
    Then "UserA" verifies that he is logged out from the App